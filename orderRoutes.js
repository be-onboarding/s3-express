const express = require("express");
const router = express.Router();
const auth = require("../auth");
const userController = require("../controllers/userController");

router.post("/checkout", auth.verify, (req, res) => {
	if(auth.decode(req.headers.authorization).isAdmin === true) {
		res.send(false);
	} else {
		const userId = auth.decode(req.headers.authorization).id;
		userController.checkout(userId, req.body).then(resultFromController => res.send(resultFromController));
	}
})
router.get("/", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin === true) {
		res.send(false);
	} else {
		userController.getMyOrders(userData.id).then(resultFromController => res.send(resultFromController));
	}
})

module.exports = router;
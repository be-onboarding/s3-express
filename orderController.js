const User = require("../models/user");
const Product = require("../models/product");

module.exports.checkout = (userId, cart) => {
	return User.findById(userId).then(user => {
		if(user === null){
			return false;
		} else {
			user.orders.push(
				{
					products: cart.products,
					totalAmount: cart.totalAmount
				}
			);
			return user.save().then((updatedUser, error) => {
				if(error){
					return false;
				} else {
					const currentOrder = updatedUser.orders[updatedUser.orders.length-1];
					
					currentOrder.products.forEach(product => {
						Product.findById(product.productId).then(foundProduct => {
							foundProduct.orders.push({orderId: currentOrder._id})

							foundProduct.save()
						})
					});

					return true;
				}
			})
		}
	})
}

module.exports.getOrders = (userId) => {
	return User.findById(userId).then(user => {
		if(user === null){
			return false;
		} else {
			return user.orders;
		}
	})
}